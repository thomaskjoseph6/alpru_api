# Base Image
# FROM nvidia/cuda:11.3.1-cudnn8-devel-ubuntu20.04
FROM ubuntu:20.04

ENV PATH="/Scripts:${PATH}"

RUN mkdir /app

# copy project
COPY ./alpr_api /app

WORKDIR /app

COPY ./Scripts /Scripts 
RUN chmod +x /Scripts/*

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PORT=80
ENV PATH /opt/conda/envs/py2-dev/bin:$PATH
ENV TZ=America/New_York

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ENV PATH="/root/miniconda3/bin:${PATH}"
ARG PATH="/root/miniconda3/bin:${PATH}"


RUN apt-get update --fix-missing && \
    apt-get install -y tzdata --no-install-recommends wget bzip2 ca-certificates libglib2.0-0 libxext6 libsm6 libxrender1 git mercurial subversion build-essential libgtk2.0-dev  && \
    apt-get clean  && \
    rm -rf /var/lib/apt/lists/* && \
    wget \
    https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && mkdir /root/.conda \
    && bash Miniconda3-latest-Linux-x86_64.sh -b \
    && rm -f Miniconda3-latest-Linux-x86_64.sh && \
    conda env create -f envname.yaml 

# Make RUN commands use the new environment:
SHELL ["conda", "run", "-n", "py2-dev", "/bin/bash", "-c"]

# Demonstrate the environment is activated:
RUN echo "Make sure flask is installed:"
RUN python -c "import flask"

EXPOSE $PORT

#CMD ["bash", "-c", "cd darknet && make && cd .. && source activate py2-dev && python main.py"]
CMD ["entrypoint.sh"]