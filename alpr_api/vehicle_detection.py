import sys
import cv2
import numpy as np
import traceback

import darknet.python.darknet as dn

from src.label 				import Label, lwrite
from os.path 				import splitext, basename, isdir
from os 					import makedirs
import os
from src.utils 				import crop_region, image_files_from_folder
from darknet.python.darknet import detect

from license_plate_detection import detect_licence_plate


def process_alpr(img_path):

    try:
        #output_dir = '/home/thomas/Thomas/Dev/Python/RAD/alpr-unconstrained/samples/Temp'
        output_dir=os.path.abspath(os.getcwd()) + '/Temp/'
        vehicle_threshold = .5

        vehicle_weights = 'data/vehicle-detector/yolo-voc.weights'
        vehicle_netcfg  = 'data/vehicle-detector/yolo-voc.cfg'
        vehicle_dataset = 'data/vehicle-detector/voc.data'

        vehicle_net  = dn.load_net(vehicle_netcfg, vehicle_weights, 0)
        vehicle_meta = dn.load_meta(vehicle_dataset)

        if not isdir(output_dir):
            makedirs(output_dir)


        bname = basename(splitext(img_path)[0])

        R,_ = detect(vehicle_net, vehicle_meta, img_path ,thresh=vehicle_threshold)
        R = [r for r in R if r[0] in ['car','bus']]

        license_values=[]

        if len(R):
            Iorig = cv2.imread(img_path)
            WH = np.array(Iorig.shape[1::-1],dtype=float)
            Lcars = []

            for i,r in enumerate(R):

                cx,cy,w,h = (np.array(r[2])/np.concatenate( (WH,WH) )).tolist()
                tl = np.array([cx - w/2., cy - h/2.])
                br = np.array([cx + w/2., cy + h/2.])
                label = Label(0,tl,br)
                Icar = crop_region(Iorig,label)

                Lcars.append(label)

                cv2.imwrite('%s/%s_%dcar.png' % (output_dir,bname,i),Icar)

                licence_val=detect_licence_plate(output_dir)

                license_values.append(licence_val)

            # lwrite('%s/%s_cars.txt' % (output_dir,bname),Lcars)
            return license_values
            
    except:
        traceback.print_exc()
        return "-1"


            

