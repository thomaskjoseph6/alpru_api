from flask import Flask,jsonify,request
from flask_restful import Api,Resource
import requests
from os.path 				import splitext, basename, isdir
import os
import traceback
from PIL import Image

from vehicle_detection import process_alpr

app=Flask(__name__)


@app.route('/',methods=['GET'])
def home():
    return jsonify({"result":"home"})

@app.route('/alpr',methods=['POST'])
def alpr():
    try:
        image_url=request.json["image_url"]
        if image_url is None:
            return jsonify({"result":"Bad Request"}),400
        
        image_folder_path=os.path.abspath(os.getcwd()) + '/upload/'
        if not isdir(image_folder_path):
            os.makedirs(image_folder_path)
            
        bname = basename(splitext(image_url)[0])
        image_path=image_folder_path + bname + '.jpg'
        img_data = requests.get(image_url).content
        with open(image_path, 'wb') as handler:
            handler.write(img_data)

        with Image.open(image_path) as im:
            im.verify()
            
        val=process_alpr(image_path)
        if val is None:
            return jsonify({"result":[]})
        else:
            return jsonify({"result":val})

    except:
        traceback.print_exc()
        return jsonify({"result":"Bad Request"}),400

if __name__=="__main__":
    app.run(debug=False,port=80,host='0.0.0.0')
