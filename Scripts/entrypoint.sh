#!/bin/sh

set -e
# cd darknet && make
# source activate py2-dev

conda env list
# conda run --no-capture-output -n py2-dev python main.py

conda run --no-capture-output -n py2-dev gunicorn --bind 0.0.0.0:80 wsgi:app --timeout 1000 --workers=4

# conda init bash
# conda activate py2-dev
# ls -al
# python main.py
# python manage.py collectstatic --noinput
# python manage.py makemigrations --noinput
# python manage.py migrate --noinput
# python manage.py generate_thumbnails

# gunicorn --log-file=/vol/web/logs/gunicorn_log.txt Website.wsgi:application --bind 0.0.0.0:9000 --workers=5
